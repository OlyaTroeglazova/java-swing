import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class TableWithSwapColumns {

    private JTable table;
    private JPanel main;

    public TableWithSwapColumns(int [][] board) {
        initJTable(board);
    }

    private void initJTable(int[][] board) {
        int height = board.length;
        int width = board[0].length;

        for (int i = 0; i<height-1; i+=2) {
            swapColumns(board, i, i+1);
        }

        Object[][] boardObject = new Object[height][width];
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++)
                boardObject[row][column] = board[row][column];
        }

        DefaultTableModel model = (DefaultTableModel) table.getModel();

        for (int column = 0; column < width; column++) {
            model.addColumn("");
        }

        for (int row = 0; row < height; row++) {
            model.addRow(boardObject[row]);
        }
        table.setModel(model);
    }

    private void swapColumns(int [][] board, int first, int second){
        for(int i = 0; i<board[0].length; i++){
            int tmp = board[i][first];
            board[i][first] = board[i][second];
            board[i][second] = tmp;
        }
    }

    public static void call(int[][] board){
        JFrame frame = new JFrame("New Table");
        frame.setContentPane(new TableWithSwapColumns(board).main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
