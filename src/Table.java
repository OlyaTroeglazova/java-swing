import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static java.lang.Integer.parseInt;

public class Table {
    private final int N = 5;
    private final int x;
    private final int[][] board;

    private JPanel main;
    private JTable table;
    private JButton button;
    private JTextField textField;
    private JButton buttonCalculate;

    public Table(int x) {
        this.x = x;
        this.board = initJTable(table);

        setValuesInAnArray(board);
        refreshJTable(table);

        table.setCellSelectionEnabled(true);
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.getSelectedRow();
                int column = table.getSelectedColumn();
                System.out.println("Selected cell: row = " + row + ", column = " + column + ", value = " + table.getValueAt(row, column));
                textField.setText(table.getValueAt(row, column).toString());
            }
        });

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int row = table.getSelectedRow();
                int column = table.getSelectedColumn();
                textField.setText(table.getValueAt(row, column).toString());
            }
        });

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int row = table.getSelectedRow();
                int column = table.getSelectedColumn();
                textField.setText(table.getValueAt(row, column).toString());
            }
        });

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                int row = table.getSelectedRow();
                int column = table.getSelectedColumn();
                int value = parseInt(textField.getText());
                board[row][column] = value;
                System.out.println("Value changed. New value = " + value);
                refreshJTable(table);
            }
        });

        buttonCalculate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                TableWithSwapColumns.call(board);
            }
        });
    }

    private int[][] initJTable(JTable table) {
        int [][] board = new int[N][N];
        // Берем размеры входного двумерного массива
        int height = board.length;
        int width = board[0].length;

        Object[][] boardObject = new Object[N][N];
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++)
                boardObject[row][column] = board[row][column];
        }

        // Берем модель, которая позволяет добавлять строки и столбцы в таблицу
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        // Нужно добавить столько столбцов, сколько у нас ширина двумерного массива
        for (int column = 0; column < width; column++) {
            model.addColumn("");
        }

        // Нужно добавить все строки из двумерного массива
        for (int row = 0; row < height; row++) {
            model.addRow(boardObject[row]);
        }
        table.setModel(model);
        return board;
    }

    private void setValuesInAnArray(int[][] board) {
        int height = board.length;
        int width = board[0].length;

        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                board[row][column] = (int) Math.pow(x, row + 1);
            }
        }
    }

    private void refreshJTable(JTable table) {
        // Обновляем таблицу столько раз - сколько необходимо
        // Обходим таблицу и заполняем ее значениями из двумерного массива
        int height = board.length;
        int width = board[0].length;

        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++)
                table.setValueAt(board[row][column], row, column);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Table");
        frame.setContentPane(new Table(6).main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}